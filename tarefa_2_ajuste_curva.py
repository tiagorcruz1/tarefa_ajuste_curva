# -*- coding: utf-8 -*-
"""
Created on Tue Apr  2 12:13:36 2019
*
* algoritmo para ajuste de curva de uma base de dados da taxa de cambio do dólar
* obtencao dos parametros de um polinomio de 5a ordem
* calculo da matriz de covariancia
* plots apresentam comparacao anual entre os valores de entrada com os valores calculados
*
* Fonte dos dados:
* https://br.investing.com/currencies/usd-brl-historical-data
*
@author: Tiago Cruz
"""
# importa modulos necessarios
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd # ler arquivos .csv
import math
from numpy.linalg import inv # inverter matrizes

plt.close("all")

# Read data from file 'filename.csv' 
# (in the same directory that your python process is based)
# Control delimiters, rows, column names with read_csv (see later)
dados = pd.read_csv("USD_BRL.csv")

# seleciona coluna de interesse
dolar = dados[["Ultimo"]]

# converte para vetor (necessario para a transposta)
dolar = np.array(dolar)

# variaveis auxiliares
amostra = 260
anos_avaliados = 9
start = 0
stop = amostra

# laco para avaliar separadamente os 9 anos
for n in range(0, anos_avaliados):
    # dados de 1 ano
    dolar_aux = dolar[start:stop]
    
    start = stop
    stop = stop + amostra
        
    # referencia de data da coleta
    t = np.linspace(1, len(dolar_aux), len(dolar_aux))
    
    # numero de parametros
    teta = 5
    
    # declara matriz phi com valores iniciais 0
    phi = np.zeros((len(dolar_aux), teta))
    
    # ciclo para definicao da matriz phi
    for i in range(0, len(dolar_aux)):
        ii=0
        for j in reversed(range(0, teta)): # maior expoente a esquerda
            phi[i,ii] = math.pow(t[i],j)
            ii = ii+1
    
    # implementacao da funcao de ajuste de curvas
    teta_chapeu = phi.T@phi
    teta_chapeu = inv(teta_chapeu)
    covariancia = teta_chapeu # armazena para posterior calculo
    teta_chapeu = teta_chapeu@phi.T
    teta_chapeu = teta_chapeu@dolar_aux
    print("teta_chapeu - " +str(2010+n),":\n", teta_chapeu, "\n")
    
    # equação da reta
    # If either a or b is 0-D (scalar), it is equivalent to multiply and using numpy.multiply(a, b) or a * b is preferred.
    f_t = teta_chapeu[0]*t*t*t*t + teta_chapeu[1]*t*t*t + teta_chapeu[2]*t*t + teta_chapeu[3]*t + teta_chapeu[4]
    
    # erro quadratico
    v_0 = np.sum((dolar_aux - f_t))
    v_0 = math.pow(v_0, 2)
    
    # calcula da covariancia
    covariancia = covariancia*2*v_0/(len(dolar_aux)-teta)
    print("Matriz de covariancia - " +str(2010+n),":\n", covariancia, "\n")
    
    # plots
    plt.figure(n+1)
#    plt.subplot(2, 1, 1)
    plt.plot(t, dolar_aux, '.')
    plt.plot(t,f_t)
    plt.title("Histórico do preço do Dólar - "+str(2010+n))
    plt.xlabel("Mês"), plt.ylabel("Preço [R$]")
    plt.xlim(0, amostra)
    plt.xticks(np.arange(0, 264, 22), ('Jan', 'Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'), rotation = 30)
    plt.legend(('Pontos observados', 'Ajuste de curva'), loc='upper right')
#    plt.subplot(2, 1, 2)
#    plt.scatter(dolar_aux, f_t)

# plot 
plt.figure(0)
plt.plot(dolar)
v_line = np.linspace(0, len(dolar), anos_avaliados+1)
for number in v_line:
    plt.axvline(number, linewidth = 0.5, color = 'black')
plt.xticks(np.arange(0, len(dolar), amostra), (2010, 2011,2012,2013,2014,2015,2016,2017,2018))
plt.xlim(0, len(dolar))
plt.title("Histórico do preço do Dólar")
plt.xlabel("Ano"), plt.ylabel("Preço [R$]")